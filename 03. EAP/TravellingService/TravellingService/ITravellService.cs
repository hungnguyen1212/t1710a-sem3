﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TravellingService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITravellService" in both code and config file together.
    [ServiceContract]
    public interface ITravellService
    {

        //Thao tac lien quan den Them Sua Xoa bai Post Place của Employee
        [OperationContract]
        List<Travel_Detail> GetTravel_Details();

        [OperationContract]
        bool addNewPlace(Travel_Detail travel_Detail);

        [OperationContract]
        bool deletePlace(int PlaceID);

        [OperationContract]
        bool modifyPlace(Travel_Detail travel_Detail);


        //Thao tac lien quan den Cmt va Rate của User
        [OperationContract]
        bool addComment(Traveller traveller);

        [OperationContract]
        bool deleteCommet(int ID);

        [OperationContract]
        bool rating(Traveller traveller);

        //Login and Sign in
        [OperationContract]
        bool signup(User user);

        [OperationContract]
        bool loginUser(User user);

        [OperationContract]
        bool loginEmployee(Employee employee);
    }
}
