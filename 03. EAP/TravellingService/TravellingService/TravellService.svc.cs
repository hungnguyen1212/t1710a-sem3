﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TravellingService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TravellService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TravellService.svc or TravellService.svc.cs at the Solution Explorer and start debugging.
    public class TravellService : ITravellService
    {
        TravellingDataDataContext data = new TravellingDataDataContext();
        public int currentUserID;
        public int currentEmployeeID;
        public bool addComment(Traveller traveller)
        {
            try
            {
                traveller.idUser = currentUserID;
                data.Travellers.InsertOnSubmit(traveller);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool addNewPlace(Travel_Detail travel_Detail)
        {
            try
            {
                data.Travel_Details.InsertOnSubmit(travel_Detail);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool deleteCommet(int ID)
        {
            try
            {
                Traveller travellerToDelete = (from traveller 
                                               in data.Travellers
                                               where traveller.idTD == ID && traveller.idUser == currentUserID
                                               select traveller).Single();
                data.Travellers.DeleteOnSubmit(travellerToDelete);
                data.SubmitChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }

        public bool deletePlace(int PlaceID)
        {
            try
            {
                Travel_Detail travel_DetailtoDelete = (from travel in data.Travel_Details where travel.idTD == PlaceID select travel).Single();
                data.Travel_Details.DeleteOnSubmit(travel_DetailtoDelete);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<Travel_Detail> GetTravel_Details()
        {
            try
            {
                return (from traveller in data.Travel_Details select traveller).ToList();
            }
            catch
            {
                return null;
            }
        }

        public bool loginEmployee(Employee employee)
        {
            try
            {
                Employee checkUser = (from userchose 
                                      in data.Employees
                                      where userchose.usernameE == employee.usernameE && userchose.passwordE == employee.passwordE
                                      select userchose).Single();
                if (checkUser != null)
                {
                    currentEmployeeID = checkUser.idEmployee;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool loginUser(User user)
        {
            try
            {
                User checkUser = (from userchose 
                                  in data.Users
                                  where userchose.Username == user.Username && userchose.Password == user.Password
                                  select userchose).Single();
                if (checkUser != null)
                {
                    currentUserID = checkUser.idUser;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch 
            {
                return false;
            }
        }

        public bool modifyPlace(Travel_Detail travel_Detail)
        {
            try
            {
                Travel_Detail travelToModify = (from travel in data.Travel_Details where travel.idTD == travel_Detail.idTD select travel).Single();
                travelToModify.place = travel_Detail.place;
                travelToModify.images = travel_Detail.images;
                travelToModify.info = travel_Detail.info;
                data.SubmitChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }

        public bool rating(Traveller traveller)
        {
            try
            {
                traveller.idUser = currentUserID;
                data.Travellers.InsertOnSubmit(traveller);
                data.SubmitChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }

        public bool signup(User user)
        {
            try
            {
                User checkUser = (from userchose in data.Users where userchose.Username == user.Username select userchose).Single();
                if (checkUser == null)
                {
                    data.Users.InsertOnSubmit(user);
                    data.SubmitChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
