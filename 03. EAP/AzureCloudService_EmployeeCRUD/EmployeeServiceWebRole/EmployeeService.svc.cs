﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EmployeeServiceWebRole
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeService.svc or EmployeeService.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeService : IEmployeeService
    {
        EmployeeDataClassesDataContext data = new EmployeeDataClassesDataContext();

        public bool addNewCustomer(Employee newEmployee)
        {
            try
            {
                data.Employees.InsertOnSubmit(newEmployee);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool deteleEmployee(int employeeID)
        {
            try
            {
                Employee employeeToDelete = (from employee in data.Employees where employee.empID == employeeID select employee).Single();
                data.Employees.DeleteOnSubmit(employeeToDelete);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool modifyEmployee(Employee newEmployee)
        {
            try
            {
                Employee employeeToModify = (from employee in data.Employees where employee.empID == newEmployee.empID select employee).Single();
                employeeToModify.Age = newEmployee.Age;
                employeeToModify.firstname = newEmployee.firstname;
                employeeToModify.lastname = newEmployee.lastname;
                employeeToModify.address = newEmployee.address;
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<Employee> GetEmployees()
        {
            try
            {
                return (from employee in data.Employees select employee).ToList();
            }
            catch
            {
                return null;
            }
        }
    }
}
