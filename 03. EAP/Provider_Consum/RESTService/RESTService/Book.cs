﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RESTService
{
    [DataContract]
    public class Book
    {
        [DataMember]
        public int BookId { get; set; }

        [DataMember]
        public string Tittle { get; set; }

        [DataMember]
        public string ISBN { get; set; }
    }


    public interface IBookRepository
    {
        List<Book> GetAllBooks();
        Book GetBookById(int id);
        Book AddNewBook(Book item);
        bool DeleteBook(int id);
        bool UpdateBook(Book item);
    }

    public class BookRepository : IBookRepository
    {
        private List<Book> books = new List<Book>();
        private int counter = 1;

        public BookRepository()
        {
            AddNewBook(new Book { Tittle = "C# Programming", ISBN = "2323232323232" });
            AddNewBook(new Book { Tittle = "Java Programming", ISBN = "123131251251" });
            AddNewBook(new Book { Tittle = "WCF Programming", ISBN = "3452154552324" });
        }

        //CRUD Operations
        //1. CREATE
        public Book AddNewBook(Book newBook)
        {
            if (newBook == null)
                throw new ArgumentNullException("newBook");
            newBook.BookId = counter++;
            books.Add(newBook);
            return newBook;
        }

        //2. RETRIEVE /ALL
        public List<Book> GetAllBooks()
        {
            return books;
        }

        //3. RETRIEVE /By BookId
        public Book GetBookById(int bookId)
        {
            return books.Find(b => b.BookId == bookId);
        }

        //4. UPDATE
        public bool UpdateABook(Book updatedBook)
        {
            if (updatedBook == null)
                throw new ArgumentNullException("updatedBook");

            int idx = books.FindIndex(b => b.BookId == updatedBook.BookId);
            if (idx == -1)
                return false;

            books.RemoveAt(idx);
            books.Add(updatedBook);
            return true;
        }

        //5. DELETE
        public bool DeteleABook(int bookId)
        {
            int idx = books.FindIndex(b => b.BookId == bookId);
            if (idx == -1)
                return false;

            books.RemoveAll(b => b.BookId == bookId);
            return true;
        }

        public bool DeleteBook(int id)
        {
            throw new NotImplementedException();
        }

        public bool UpdateBook(Book item)
        {
            throw new NotImplementedException();
        }
    }
}