﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace RESTService
{
    public class BookServices : IBookServices
    {
        static IBookRepository repository = new BookRepository();
        public string AddBook(Book book, string id)
        {
            Book newBook = repository.AddNewBook(book);
            return "id=" + newBook.BookId;
        }

        public string DeleteBook(string id)
        {
            bool deleted = repository.DeleteBook(int.Parse(id));
            if (deleted)
                return "Book with id " + id + " deleted Successfully";
            else
                return "Unable to update book with id = " + id;
        }

        public Book GetBookById(string id)
        {
            return repository.GetBookById(int.Parse(id));
        }

        public List<Book> GetBooksList()
        {
            return repository.GetAllBooks();
        }

        public string UpdateBook(Book book, string id)
        {
            bool updated = repository.UpdateBook(book);
            if (updated)
                return "Book with id = " + id + " updated successfully";
            else
                return "Unable to update book with id = " + id;
        }
    }
}