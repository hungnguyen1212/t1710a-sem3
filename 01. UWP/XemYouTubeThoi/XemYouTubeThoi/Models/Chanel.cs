﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XemYouTubeThoi.Models
{
    public class Chanel
    {
        public string ChId { get; set; }
        public string ChanelTittle { get; set; }
        public string ChanelImg { get; set; }

        public static ObservableCollection<Chanel> GetChanels()
        {
            var chanels = new ObservableCollection<Chanel>();

            chanels.Add(new Chanel
            {
                ChId = "UC6k5wSE9XCfDge4Sgxn5rIA",
                ChanelTittle = "Hannah Hoang",
                ChanelImg = "https://yt3.ggpht.com/a-/AAuE7mAqGP08E2De5kPDoXGbFR3w35M2j93V3WIxKg=s288-mo-c-c0xffffffff-rj-k-no"
            });

            chanels.Add(new Chanel
            {
                ChId = "UCUtZaxDF3hD5VK4xRYFBePQ",
                ChanelTittle = "Britain's Got Talent",
                ChanelImg = "https://yt3.ggpht.com/a-/AAuE7mDLojvp7f-S799LPTmRqSWXn98faSelGLQ7hQ=s288-mo-c-c0xffffffff-rj-k-no"
            });

            chanels.Add(new Chanel
            {
                ChId = "UCCSq5ajyn18IP1YM54u0onA",
                ChanelTittle = "Top Khám Phá",
                ChanelImg = "https://yt3.ggpht.com/a-/AAuE7mBqp7s-jGRwqJxdbtoE5vbPQXkkDbL_BWDkwg=s288-mo-c-c0xffffffff-rj-k-no"
            });

            return chanels;
        }
    }
}
