﻿using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Navigation;
using XemYouTubeThoi.Models;
using SQLite;
using SQLitePCL;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace XemYouTubeThoi
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        List<Video> ListVideo = new List<Video>();
        List<Favorite> ListFavorite = new List<Favorite>();
        public ObservableCollection<Chanel> ListChanel;
        private string TokenNextPage = null, TokenPrivPage = null;

        private YouTubeService youtubeService = new YouTubeService(new BaseClientService.Initializer
        {
            ApiKey = "AIzaSyDBf8bq5AKUSHfF_CF0eeZ2RCLzyfmOi5s",
            ApplicationName = "youtube"

        });

        

        public MainPage()
        {
            this.InitializeComponent();
            ListChanel = Chanel.GetChanels();
            
            
        }

        

        private async void GetVideo(string ChId, string PageToken = null)
        {
            try
            {
                if (NetworkInterface.GetIsNetworkAvailable())
                {
                    var Request = youtubeService.Search.List("snippet");
                    Request.ChannelId = ChId;
                    Request.MaxResults = 25;
                    Request.Type = "video";
                    Request.Order = SearchResource.ListRequest.OrderEnum.Date;
                    Request.PageToken = PageToken;
                    var Result = await Request.ExecuteAsync();
                    if (Result.NextPageToken != null)
                    {
                        TokenNextPage = Result.NextPageToken;
                    }
                    if (Result.PrevPageToken != null)
                    {
                        TokenPrivPage = Result.PrevPageToken;
                    }

                    ListVideo.Clear();

                    foreach (var item in Result.Items)
                    {
                        ListVideo.Add(new Video
                        {
                            Tittle = item.Snippet.Title,
                            Id = item.Id.VideoId,
                            Img = item.Snippet.Thumbnails.Default__.Url
                        });
                    }
                    lv.ItemsSource = null;
                    lv.ItemsSource = ListVideo;
                }
                else
                {
                    MessageDialog msg = new MessageDialog("Check your internet connection");
                    await msg.ShowAsync();
                }
            }
            catch { }
        }

        //private void lc_ItemClick(object sender, ItemClickEventArgs e)
        //{
        //    var choose = (Chanel)e.ClickedItem;
        //    GetVideo(choose.ChId);
        //}

        private void ListCh_ItemClick(object sender, ItemClickEventArgs e)
        {
            var choose = e.ClickedItem as Chanel;
            GetVideo(choose.ChId);
            
        }

        private void btn_favorite_Click(object sender, RoutedEventHandler e)
        {
            var chooseChanel = e.Target as Chanel;
            var chooseVideo = e.Target as Video;
            ListFavorite.Add(new Favorite {
                fv_ChId = chooseChanel.ChId,
                fv_Id = chooseVideo.Id,
                fv_Tittle = chooseVideo.Tittle,
                fv_Img = chooseVideo.Img
            });
        }

        private void favorite_list_Click(object sender, RoutedEventArgs e)
        {
            GetFavoriteVideo();
        }


        //private void cbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var choose = cbx.SelectedItem as Chanel;
        //    GetVideo(choose.ChId);
        //}

        private void lv_ItemClick(object sender, ItemClickEventArgs e)
        {
            Video video = e.ClickedItem as Video;
            Frame.Navigate(typeof(VideoPage), video);
        }

        private async void GetFavoriteVideo()
        {
            try
            {
                if (NetworkInterface.GetIsNetworkAvailable())
                {                  
                    lv.ItemsSource = null;
                    lv.ItemsSource = ListChanel;
                }
                else
                {
                    MessageDialog msg = new MessageDialog("Check your internet connection");
                    await msg.ShowAsync();
                }
            }
            catch { }
        }
    }
}
