﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Calculator
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        public void Number0_Click(object sender, RoutedEventArgs e)
        {
            if (TextBlockMath.Text.Equals(""))
            {
                TextBlockFirstNumber.Text = TextBlockFirstNumber.Text + "0";
            } else
            {
                TextBlockSecondNumber.Text = TextBlockSecondNumber.Text + "0";
            }
        }

        public void Number1_Click(object sender, RoutedEventArgs e)
        {
            if (TextBlockMath.Text.Equals(""))
            {
                TextBlockFirstNumber.Text = TextBlockFirstNumber.Text + "1";
            }
            else
            {
                TextBlockSecondNumber.Text = TextBlockSecondNumber.Text + "1";
            }
        }

        public void Number2_Click(object sender, RoutedEventArgs e)
        {
            if (TextBlockMath.Text.Equals(""))
            {
                TextBlockFirstNumber.Text = TextBlockFirstNumber.Text + "2";
            }
            else
            {
                TextBlockSecondNumber.Text = TextBlockSecondNumber.Text + "2";
            }
        }

        public void Number3_Click(object sender, RoutedEventArgs e)
        {
            if (TextBlockMath.Text.Equals(""))
            {
                TextBlockFirstNumber.Text = TextBlockFirstNumber.Text + "3";
            }
            else
            {
                TextBlockSecondNumber.Text = TextBlockSecondNumber.Text + "3";
            }
        }

        public void Number4_Click(object sender, RoutedEventArgs e)
        {
            if (TextBlockMath.Text.Equals(""))
            {
                TextBlockFirstNumber.Text = TextBlockFirstNumber.Text + "4";
            }
            else
            {
                TextBlockSecondNumber.Text = TextBlockSecondNumber.Text + "4";
            }
        }

        public void Number5_Click(object sender, RoutedEventArgs e)
        {
            if (TextBlockMath.Text.Equals(""))
            {
                TextBlockFirstNumber.Text = TextBlockFirstNumber.Text + "5";
            }
            else
            {
                TextBlockSecondNumber.Text = TextBlockSecondNumber.Text + "5";
            }
        }

        public void Number6_Click(object sender, RoutedEventArgs e)
        {
            if (TextBlockMath.Text.Equals(""))
            {
                TextBlockFirstNumber.Text = TextBlockFirstNumber.Text + "6";
            }
            else
            {
                TextBlockSecondNumber.Text = TextBlockSecondNumber.Text + "6";
            }
        }

        public void Number7_Click(object sender, RoutedEventArgs e)
        {
            if (TextBlockMath.Text.Equals(""))
            {
                TextBlockFirstNumber.Text = TextBlockFirstNumber.Text + "7";
            }
            else
            {
                TextBlockSecondNumber.Text = TextBlockSecondNumber.Text + "7";
            }
        }

        public void Number8_Click(object sender, RoutedEventArgs e)
        {
            if (TextBlockMath.Text.Equals(""))
            {
                TextBlockFirstNumber.Text = TextBlockFirstNumber.Text + "8";
            }
            else
            {
                TextBlockSecondNumber.Text = TextBlockSecondNumber.Text + "8";
            }
        }

        public void Number9_Click(object sender, RoutedEventArgs e)
        {
            if (TextBlockMath.Text.Equals(""))
            {
                TextBlockFirstNumber.Text = TextBlockFirstNumber.Text + "9";
            }
            else
            {
                TextBlockSecondNumber.Text = TextBlockSecondNumber.Text + "9";
            }
        }

        public void Addition_Click(object sender, RoutedEventArgs e)
        {
            TextBlockMath.Text = "+";
        }

        public void Subtraction_Click(object sender, RoutedEventArgs e)
        {
            TextBlockMath.Text = "-";
        }

        public void Multiplication_Click(object sender, RoutedEventArgs e)
        {
            TextBlockMath.Text = "x";
        }

        public void Division_Click(object sender, RoutedEventArgs e)
        {
            TextBlockMath.Text = "/";
        }

        public void Result_Click(object sender, RoutedEventArgs e)
        {
            if (TextBlockMath.Text.Equals("+"))
            {
                TextBlockResult.Text = (Double.Parse(TextBlockFirstNumber.Text) + Double.Parse(TextBlockSecondNumber.Text)).ToString();
            } else if (TextBlockMath.Text.Equals("-"))
            {
                TextBlockResult.Text = (Double.Parse(TextBlockFirstNumber.Text) - Double.Parse(TextBlockSecondNumber.Text)).ToString();
            } else if (TextBlockMath.Text.Equals("x"))
            {
                TextBlockResult.Text = (Double.Parse(TextBlockFirstNumber.Text) * Double.Parse(TextBlockSecondNumber.Text)).ToString();
            } else if (TextBlockMath.Text.Equals("/"))
            {
                TextBlockResult.Text = (Double.Parse(TextBlockFirstNumber.Text) / Double.Parse(TextBlockSecondNumber.Text)).ToString();
            }
        }

        public void Dot_Click(object sender, RoutedEventArgs e)
        {

        }

        public void Delete_Click(object sender, RoutedEventArgs e)
        {

        }

        public void C_Click(object sender, RoutedEventArgs e)
        {
            TextBlockFirstNumber.Text = "";
            TextBlockMath.Text = "";
            TextBlockSecondNumber.Text = "";
            TextBlockResult.Text = "";
        }

        public void CE_Click(object sender, RoutedEventArgs e)
        {

        }

        public void Addition_Subtraction_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
