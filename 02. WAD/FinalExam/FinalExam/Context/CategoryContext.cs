﻿using FinalExam.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FinalExam.Context
{
    public class CategoryContext : DbContext
    {
        public DbSet<Category> Products { get; set; }
    }
}